<?php
namespace System;

/**
* @author Ammar Faizi <ammarfaizi2@gmail.com>
*/
class Database
{
	
	public function __construct($driver,$host,$user,$pass,$db)
	{
		try {
			$this->db = new \PDO($driver.":host=".$host.";dbname=".$db,$user,$pass);	
			$this->db->setAttribute(PDO::ERRMODE_EXCEPTION); 
		} catch (\PDOException $e) {
			#print "Gagal Terhubung ke Database !";
		}
	}
	public function get_connection()
	{
		return $this->db;
	}
}