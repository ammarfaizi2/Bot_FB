<?php
namespace System;
use System\Database;
/**
* 
*/
class Manager
{
	
	public function __construct($db)
	{
		$this->db = (new Database('mysql',$db['host'],$db['user'],$db['pass'],$db['db']))->get_connection();
	}
	public function shower()
	{
		$st = $this->db->prepare("SELECT * FROM `res_ai`;");
		$st->execute();
		?>
		<!DOCTYPE html>
		<html>
		<head>
			<title></title>
			<style type="text/css">
				body{
					font-family: Helvetica;
				}
				.fr{
					display: inline-block;
					margin-left: 1%;
					margin-right: 1%;

				}
				.parcg{
					margin-bottom: 5%;
				}
				.sb{
					margin-top: 1%;
				}
				td{
					padding: 5px;
				}
			</style>
		</head>
		<body>
		<center>
		<form method="post" action="">
		<div class="parcg">
			<h2>Tambahkan Balasan</h2>

			<div class="fr">
				<span>Pesan : </span><br>
				<textarea required name="in"></textarea>
			</div>

			<div class="fr">
				<span>Balasan : </span><br>
				<textarea required name="rep"></textarea>
			</div>
			<div class="sb">
				<input type="submit" name="submit" value="Simpen">
			</div>
		</div>
		</form>
		<table style="border: 2px solid black; border-collapse: collapse;" border="1">
			<thead><tr><th>No.</th><th>Text</th><th>Balasan</th><th>Waktu pembuatan</th><th>Aksi</th></tr></thead>
			<tbody>
				<?php $num = 1;
					while ($r=$st->fetch(\PDO::FETCH_ASSOC)) {
						?>
						<tr><td align="center"><?php print $num++; ?></td><td align="center"><?php print $r['text']; ?></td><td align="center"><?php print $r['reply']; ?></td><td align="center"><?php print $r['tanggal_dibuat']; ?></td><td align="center">&nbsp;&nbsp;&nbsp;<a href="?delete=act_trr&hash=<?php print $r['hash'];?>">delete</a>&nbsp;&nbsp;&nbsp;</td></tr>
						<?php
					}
				?>
			</tbody>
		</table>
		</center>
		</body>
		</html>
		<?php
	}
	public function simpan()
	{
		$hashh = md5($_POST['in']);
		$st = $this->db->prepare("SELECT COUNT(`hash`) FROM `res_ai` WHERE `hash`=:hash LIMIT 1;");
		$st->execute(array(':hash'=>$hashh));
		$st = $st->fetch(\PDO::FETCH_NUM);
		if((bool)$st[0]){
			?>
			<script type="text/javascript">alert("Pesan sudah ada !");</script>
			<?php
		} else {
			$this->db->prepare("INSERT INTO `res_ai` (`id`,`text`,`reply`,`tanggal_dibuat`,`hash`) VALUES (null,:ttext,:reply,:tgll,:hash);")->execute(array(
					':ttext'=>trim($_POST['in']),
					':reply'=>$_POST['rep'],
					':tgll'=>(date('Y-m-d H:i:s')),
					':hash'=>$hashh
			));	
		}
	}
	public function run()
	{
		if (isset($_POST['submit'])) {
			$this->simpan();
			?>
			<script type="text/javascript">window.location='?'</script>
			<?php
		}
		if (isset($_GET['delete'])) {
			var_dump( $this->db->prepare("DELETE FROM `res_ai` WHERE `hash`=:hash LIMIT 1;")->execute(array(':hash'=>trim($_GET['hash']))));
			header('location:?ref=del&val='.$_GET['delete']);
		}
		$this->shower();
	}
}

