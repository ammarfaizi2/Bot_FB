<?php
namespace System;
use System\Database;
/**
* 
*/
class AIDB
{
	
	public function __construct($db)
	{
		$this->db = (new Database('mysql',$db['host'],$db['user'],$db['pass'],$db['db']))->get_connection();
	}
	public function prepare($msg,$actor)
	{
		$msg = strip_tags(preg_replace("#[^[:print:]]#", '', $msg));
		$this->absmsg	= $msg;
		$this->msg 		= strtolower($msg);
		$this->actor	= $actor;
		return $this;
	}
	public function check_user_chat()
	{
		$st = $this->db->prepare("SELECT `exp`,`text`,`id` FROM `chat_data` WHERE `actor`=:actor AND `text`=:ttext LIMIT 1");
		$st->execute(array(
				':actor'	=> $this->actor,
				':ttext'		=> $this->absmsg
			));
		$res = $st->fetch(\PDO::FETCH_NUM);
		$now = time();
		if (isset($res[0],$res[1])) {
			if ($res[0]<$now or $this->absmsg!=$res[1]) {
				return true;
				$this->db->prepare("DELETE FROM `chat_data` WHERE id=:idd LIMIT 1;")->execute(array(':idd'=>$r[2]));
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
	private function save_chat()
	{
		$st = $this->db->prepare("INSERT INTO `chat_data` (`id`,`actor`,`text`,`time`,`exp`,`hash`) VALUES (null,:actor,:ttext,:timee,:exp,:hash);");
		$now = time();
		$st->execute(array(
				':actor'=>$this->actor,
				':ttext'=>$this->absmsg,
				':timee'=>$now,
				':exp'	=>($now+(3600)),
				':hash' =>(md5($this->absmsg))
			));
	}
	public function execute()
	{
		if ($this->check_user_chat()) {
			$this->save_chat();	
		}
		$st = $this->db->prepare("SELECT `id`,`text`,`reply` FROM `res_ai`;");
		$st->execute();
		$data = $st->fetchall(\PDO::FETCH_NUM);
		$max_sim = array();
		foreach ($data as $key => $value) {
			similar_text($this->msg, strtolower($value[1]), $percent);
			$max_sim[$key] = $percent;
		}
		$max_val = max($max_sim);
		if ($max_val<50) {
			return false;
		} else {
			$this->reply = $data[array_search($max_val, $data)][2];
			return true;
		}
	}
	public function fetch_reply()
	{
		return $this->reply;
	}
}