-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `chat_data`;
CREATE TABLE `chat_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actor` varchar(225) NOT NULL,
  `text` text NOT NULL,
  `time` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `hash` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `res_ai`;
CREATE TABLE `res_ai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `reply` text NOT NULL,
  `tanggal_dibuat` datetime NOT NULL,
  `hash` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2017-05-07 07:40:29
