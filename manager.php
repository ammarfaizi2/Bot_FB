<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config.php';
use System\Manager;
$app = new Manager($config['database']);
$app->run();