<?php
require __DIR__.'/vendor/autoload.php';
require __DIR__.'/config.php';
use System\AIDB;
use System\Install;
use System\Facebook;
use System\BotFacebook;
use System\ChatController;

define('data', __DIR__.'/data');
define('fb_data', data.'/fb_data');
is_dir(data) or mkdir(data);
is_dir(fb_data) or mkdir(fb_data);
header('Content-type:text/plain');
$ins = new Install();
if (!$ins->is_installed()) {
    $ins->install();
}
unset($ins);


/*/ debugging here
$a = new AIDB($config['database']);
$st = $a->prepare("halo","Ammar Faizi");
if($st->execute()){
	var_dump($st->fetch_reply());
} else {
	print "gagal";
}
print PHP_EOL;
die;
//*/

$app = new BotFacebook($config);
$app->run();
